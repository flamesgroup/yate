CREATE TYPE pattern_type AS ENUM ('A', 'B');

CREATE TABLE prefixes
(
  id serial primary key,
  route text DEFAULT '-'::text,
  error text DEFAULT 'channel-congestion'::text,
  pattern text,
  pattern_type pattern_type
);

ALTER TABLE prefixes OWNER TO antrax;
ALTER SEQUENCE prefixes_id_seq RESTART WITH 1000;

