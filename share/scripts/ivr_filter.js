/**
 * IVR menu script. Purpose of this script is to filter out automoted calls.
 * For this module to work "pbx" module should be loaded and enabled
 *
 */



/******************************************** Global parameters *****************************************************************/

/* Call prefixes mapping to specified directory and DTMF pattern*/
ivr_mapping = [
    {
        prefix: ".*",
        directory: "/opt/yate/share/yate/ivr/eng/",     // directory containing ivr files from 0 to 9 and welcome in SLIN format with slin extension
        sequence_length: 1,                             // number of symbols in IVR promt
        max_ring_time: 5000                             // maximum time in milliseconds to stay in IVR after "welcome" prompt. If no valid input during this time call will be dropped
    },
];

/**
 * List of call objects;
 * Call object:
 * {
 *  ivr_group - record from ivr_mapping
 *  dumb_id - id of dumb_channel
 *  incomming_id - id of incomming call channel
 *  sequence - random dtmf sequence
 *
 * }
 */

active_calls = {};


/*********************************************************** Service functions ****************************************************/


/**
 * Attaches wave player to specified channel
 *
 * @param {string} channel_id - desired channel id
 * @param {string} wave_path - path to wavefile
 * @param {string} notify_id - id which will be used in notify message
 * @param {bool} should_ring - if true call.ringing will be emmited
 * @param {bool} repeat - if true record will repeat endlessly
 */

function attachWave(channel_id, wave_path, notify_id, should_ring, repeat) {
    var m = new Message("chan.masquerade");
    m.message = "chan.attach";
    m.id = channel_id;
    m.source = "wave/play/" + wave_path;
    m.consumer = "wave/record/-";
    m.autorepeat = !!repeat;
    if (notify_id) {
        m.notify = notify_id;
    }
    result = m.dispatch();

    if (should_ring) {
        m = new Message("chan.masquerade");
        m.message = "call.progress";
        m.id = channel_id;
        m.rtp_forward = true;
        m.earlymedia = true;
        result = m.dispatch();
    }
}

function dropCall(call, reason) {
    var m = new Message("call.drop");
    m.id = call.incomming_id;
    m.reason = reason;
    result = m.dispatch();
}

function generateRandomSeq(len) {
    seq = [];
    for (var i = 0; i < len; i++) {
        seq.push(Math.random(0,10));
    }
    return seq;
}

function sendCallForward(call) {
    m = new Message("call.forward");
    m.id = call.incomming_id;
    m.partyid = call.dumb_id;
    m.called = call.called;
    m.caller = call.caller;
    result = m.dispatch();

    if (!result || m.retValue() == "-") {
        dropCall(call, "rejected");
    }

}

/************************************************************* Message handlers *****************************************************/

/**
* Routing handler. Route all calls to dubchan
*
*/
function onRoute(msg) {
    if (!active_calls[msg.id] && !msg.id.startsWith("dumb")) {
        for (var i = 0; i < ivr_mapping.length; i++) {
            rule = new RegExp(ivr_mapping[i].prefix, "i");
            if (rule.test(msg.called)){
                call = {};
                call.incomming_id = msg.id;
                call.caller    = msg.caller;
                call.called    = msg.called;
                call.formats   = msg.formats;
                call.end_time  = 99999999999999999;
                call.ivr_group = ivr_mapping[i];
                call.dumb_id   = ""; 
                call.sequence  = generateRandomSeq(ivr_mapping[i].sequence_length);
                active_calls[msg.id] = call;

                Engine.debug(Engine.DebugInfo, "Routing #" + msg.called + " to IVR menu");
                msg.retValue("dumb/");
                return true;
            }
        }
        Engine.debug(Engine.DebugInfo, "No matching prefix for call to " + msg.called + " via " + msg.id);
        return false;
    }
}

/**
* For all calls routed to dumbchan we need to disable autoring. We'll send "180 Ringing" when we'll ready
*/
function onExecute(msg) {
    if (active_calls[msg.id]) {
        msg.autoring = false;
    }
    return false;
}

/**
* We'll receive chan.connected when dumbchan will be established and connected to incomming call channel
*
*/
function onConnected(msg) {
    if (active_calls[msg.peerid] && active_calls[msg.peerid].dumb_id == "") {
        call = active_calls[msg.peerid];
        call.dumb_id = msg.id;
        attachWave(msg.id, call.ivr_group.directory + "welcome.slin", msg.peerid, true);
    }
    return false;
}

/**
 * We will be notified when "Welcome" promt ends
 */
function onNotify(msg) {
    if (active_calls[msg.targetid]) {
        switch (msg.reason) {
            case 'eof': {
                call = active_calls[msg.targetid];
                digit = call.sequence[call.sequence.length - 1];
                attachWave(msg.id, call.ivr_group.directory + digit + ".slin", "");
                call.end_time = Date.now() + call.ivr_group.max_ring_time;
                return true;
            }
        }
    }
}

function onDtmf(msg) {
    if (active_calls[msg.id]) {
        call = active_calls[msg.id];
        digit = call.sequence.pop();
        if (digit != msg.text) {
            dropCall(call, "congestion");
            delete active_calls[msg.id];
        } else if (call.sequence.length == 0) {
            sendCallForward(call);
            delete active_calls[msg.id];
            return true;
        } else {
            digit = call.sequence[call.sequence.length - 1];
            attachWave(call.dumb_id, call.ivr_group.directory + digit + ".slin", "");
        }
        return true;
    }
    return false;
}

function onTimer(msg){
    for (call_id in active_calls) {
        call = active_calls[call_id];
        if ((call.end_time - Date.now()) < 0) {
            Engine.debug(Engine.DebugInfo, "Call to " + call.called +" ended via timeout: " + call.end_time + "|" + Date.now());
            delete active_calls[call.incomming_id];
            dropCall(call, "congestion");
            delete call;
        }
    }
}


Engine.debugName("IvrFilter");
Message.trackName(Engine.debugName());

Message.install(onRoute, "call.route", 2);
Message.install(onExecute, "call.execute", 11);
Message.install(onConnected, "chan.connected", 11);
Message.install(onNotify, "chan.notify", 11);
Message.install(onTimer, "engine.timer", 11);
/*Priority of this message should be higher than 10 - priority of "chan.masquerade" handler*/
Message.install(onDtmf, "chan.dtmf", 11);

