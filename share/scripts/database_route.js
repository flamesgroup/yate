query_1 = "SELECT * FROM prefixes WHERE (pattern_type = 'A' AND '";
query_2 = "' like pattern) OR (pattern_type = 'B' AND '";
query_3 = "' like pattern) order by id asc LIMIT 1";

function onRoute(msg) {
    var m = new Message("database");
    m.account = "yate_cdr";
    m.query = query_1 + msg.caller.sqlEscape() + query_2 + msg.called.sqlEscape() + query_3;
    Engine.debug(Engine.DebugInfo, "[" + msg.id + "] Performing query: [" + m.query + "]");
    if (m.dispatch()) {
        if (m.rows > 0) {
                route = m.getColumn("route")[0];
                if (route == "+") {
                    Engine.debug(Engine.DebugInfo, "[" + msg.id + "] Call [" + msg.caller + "->" + msg.called + "] passed");
                    return false;
                }
                msg.error = m.getColumn("error")[0];
                if (m.getColumn("reason") == null || m.getColumn("reason")[0] == 'null') {
                        msg.reason = msg.error;
                } else {
                        msg.reason = m.getColumn("reason")[0];
                }
                msg.retValue(route);
                Engine.debug(Engine.DebugInfo, "[" + msg.id + "] Call [" + msg.caller + "->" + msg.called + "] rejected");
                return true;
        }
    }
    return false;
}

Engine.debugName("DatabaseRouter");
Message.trackName(Engine.debugName());
Message.install(onRoute, "call.route", 3);
